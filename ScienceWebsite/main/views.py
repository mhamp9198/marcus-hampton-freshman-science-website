from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect


from matplotlib import pyplot

import geopandas
from shapely.geometry import shape, Point
from shapely import wkt

from .models import Earthquake
from .forms import MapForm

# functions

def scrape_and_make(year, month, day): # scrapes URL for earthquake data, and creates models of them
    year=year
    month=month
    day=day

    URL = f'https://earthquake.usgs.gov/fdsnws/event/1/query.geojson?starttime={year}-{month}-{day}%2000:00:00&endtime=2022-04-21%2023:59:59&maxlatitude=36.721&minlatitude=33.66&maxlongitude=-74.966&minlongitude=-84.988&minmagnitude=0&orderby=time'
    geojson_file = geopandas.read_file(URL) # returns a GeoDataFrame object


    '''for index, row in geojson_file.iterrows():

        the_model = Earthquake(
            magnitude = int(row.mag),
            latitude = int(row.geometry.y),
            longitude = int(row.geometry.x),
            time = int(row.time)
        )

        the_model.save() #if the model is not unique there should be a django integrity error thrown, but im not sure if this will prevent the other models from being saved
'''

#Raschka, Sebastian. “Scientific Computing in Python: Introduction to Numpy and Matplotlib.” sebastianraschka 27 Sep. 2020
	#https://sebastianraschka.com/blog/2020/numpy-intro.html/
    geometries = geojson_file.geometry
    return geometries


# Create your views here.


def home(request, year, month, day):

    

    hickory_dict_location = {'geometry': [Point(-81.3, 35.7)]}
    hickory_gdf = geopandas.GeoDataFrame(hickory_dict_location, crs="EPSG:4326")


    '''for Earthquake in Earthquake.objects.all():
        earthquake_list.append(Earthquake.magnitude)'''


    nc_shapefile_url = 'https://www2.census.gov/geo/tiger/TIGER2018/COUSUB/tl_2018_37_cousub.zip'
    nc_shapefile = geopandas.read_file(nc_shapefile_url)
    #Breuss, Martin. “Beautiful Soup: Build a Web Scraper With Python.” Real Python, Real Python, 10 Nov. 2021,	 
#realpython.com/beautiful-soup-web-scraper-python/. 

    
    fig, ax = pyplot.subplots(figsize = (10,5))
    nc_shapefile.plot(ax=ax, color='blue')
    scrape_and_make(year, month, day).plot(ax=ax, color='yellow')
    hickory_gdf.plot(ax=ax, color='red')
    pyplot.text(-81.3, 35.5, 'Hickory', color='red')
    ax.set_title(f'Earthquakes Around NC Since {month}/{day}/{year}')
    ax.set_facecolor('lightgrey')

    test = pyplot.savefig('static/da_picture.png')

    earthquake_list = nc_shapefile

    form = MapForm(request.POST or None)

    if request.method == 'POST':
        print('da method is post')
        if form.is_valid():
            print('da form is valid')
            
            year = request.POST['year']
            month = request.POST['month']
            day = request.POST['day']
            print(day)

            return HttpResponseRedirect(redirect_to=f'/{year}/{month}/{day}') 

    context = {'earthquake_list': earthquake_list, 'MapForm': MapForm}
    return render(request, 'main/home.html', context)




def redirect(request):
    return HttpResponseRedirect(redirect_to='/2022/02/02')
