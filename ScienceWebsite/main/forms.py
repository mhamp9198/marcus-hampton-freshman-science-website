from django import forms

class MapForm(forms.Form):
        year = forms.IntegerField(label='Year')
        month = forms.IntegerField(label='Month')
        day = forms.IntegerField(label='Day')

