from django.db import models

# Create your models here.


class Earthquake(models.Model):
    magnitude = models.IntegerField(default=0)
    latitude = models.FloatField(default=0)
    longitude = models.FloatField(default=0)
    time = models.BigIntegerField(default=0, unique=True)
