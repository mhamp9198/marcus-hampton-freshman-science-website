# Generated by Django 4.0.3 on 2022-04-14 15:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_remove_earthquake_distance_alter_earthquake_location_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='earthquake',
            name='location',
        ),
        migrations.AddField(
            model_name='earthquake',
            name='latitude',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='earthquake',
            name='longitude',
            field=models.IntegerField(default=0),
        ),
    ]
