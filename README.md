My science project, about earthquakes

Extensive Tutorial On Running This Locally:

1. Go to https://gitlab.com/mhamp9198/marcus-hampton-freshman-science-website (the current page being viewed)

2. In the top right corner of the webpage, create or register an account with GitLab, which can be easily done with Google or Github

3. Go back to https://gitlab.com/mhamp9198/marcus-hampton-freshman-science-website

4. Underneath the project title, on the right side of the webpage, locate the dropdown menu that says "Web IDE".

5. Open the dropdown menu, click "gitpod" to select it. Then, click the button that now says "gitpod" again.

6. An popup will appear, asking you to "Enable Gitpod?". Accept.

7. Open a seperate tab by click the button that says "gitpod" again (See Step 5).

8. You should be redirected to a new tab. Click the button in the center that says "continue with gitlab".

9. Allow gitpod.io to use your account.

10. You should now be put into an Integrated Development Environment, or IDE. In the center of the IDE, you should see a large selection, that says 'TERMINAL'. Inside the terminal, type these lines of code in, press enter, and repeat through these instructions:

    cd ScienceWebsite

    python3 -m venv venv

    source venv/bin/activate

    pip install -r requirements.txt

11. After you have installed of those packages, go to ScienceWebsite/ScienceWebsite/settings.py, and open it.

12. Inside the file, near the middle, scroll down to CSRF_TRUSTED_ORIGINS = []

13. Inside the brackets, copy and paste the URL at the top of your gitpod tab, and add quotes to both sides of your URL so that it turns red, so it looks like this:

    CSRF_TRUSTED_ORIGINS = ["https://your-URL/"]

14. Next, write '8000-' at the beginning of the URL. Remove the backslash at the end of the URL, as well. It should look like this:

    CSRF_TRUSTED_ORIGINS = ["https://8000-your-URL"]

15. Finally, back in the terminal, type:

    python manage.py runserver

16. This will you a unimportant paragraph of text, save for the 4th line of the second paragraph. Here you should see "http://127.0.0.1:8000/". Follow this link in your browser.

17. Congratulations! The website should now be running locally on your computer.



